﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RavioKB.Utilities;


namespace RavioKBTests
{
    [TestClass]
    public class ConfigManagerTest
    {
        [TestMethod]
        public void ReadFileTest()
        {
            Assert.AreEqual("ciao", ConfigManager.ReadFile(ConfigManager.fileInfo));
        }
    }
}
