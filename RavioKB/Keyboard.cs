﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace RavioKB
{
    public partial class Keyboard : Form
    {
        #region Costanti stile window
        const int WS_EX_NOACTIVATE = 0x08000000;
        #endregion

        public Keyboard()
        {
            CustomInitializeComponent();
        }


        #region Serie di funzioni per scrivere su altra applicazione

        ///Magia nera che disbilita il focus della RavioKB
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams param = base.CreateParams;
                param.ExStyle |= WS_EX_NOACTIVATE;
                return param;
            }
        }
        #endregion

        #region Metodi Pubblici

        #endregion

        #region Handler per eventi
        /// <summary>
        /// Handler che invia i caratteri all'applicazione selezionata
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void OnButtonClick(object sender, EventArgs e)
        {
            if (sender is RavioButton)
            {
                SendKeys.SendWait((sender as RavioButton).ToKey());
                
            }
            else
            {
                throw new Exception();
            }
        }

        public static void OnDisposingLayer(object sender, EventArgs e)
        {
            /*if (sender is Layer)
            {
                //(sender as Layer).Invalidate();
                //(sender as Layer).Parent.Invalidate();
                //(sender as Layer).BaseLayer.Invalidate();
                ((sender as Layer).BaseLayer.Parent).Refresh();
            }*/
        }



        #endregion
    }
}
