﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;


namespace RavioKB.Utilities
{
    public static class ConfigManager
    {
        public static Config config = new Config();
        public static List<SerializeButton> buttonlist = new List<SerializeButton>();

        public static string Serialize()
        {
            return JsonConvert.SerializeObject(config,Formatting.Indented);
        }

        public static string SerializeButtons()
        {
            return JsonConvert.SerializeObject(buttonlist, Formatting.Indented);
        }


        public static void Deserialize(string json)
        {
            config = JsonConvert.DeserializeObject<Config>(json);
        }

        public static void DeserializeButton(string json)
        {
            buttonlist = JsonConvert.DeserializeObject<List<SerializeButton>>(json);
        }

        public static string ReadFile(string filename)
        {
            String text ="";
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(filename+".txt");
                //Read the first line of text
                text = sr.ReadToEnd();
                //close the file
                sr.Close();
                return text;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                return text;
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }

        public static void WriteFile(string text, string filename)
        {
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                StreamWriter sw = new StreamWriter(filename+".txt");
                //Read the first line of text
                sw.WriteLine(text);
                //close the file
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }

    }
}
