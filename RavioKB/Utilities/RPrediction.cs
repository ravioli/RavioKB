﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RavioKB.Properties;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.IO;

namespace RavioKB.Utilities
{
    public class RPrediction
    {
        /// <summary>
        /// lista di parole estratte da un file
        /// </summary>
        public List<string> words = new List<string>();
        /// <summary>
        /// lista di elementi Sstring calcolata dalle words
        /// </summary>
        public List<Sstring> list = new List<Sstring>();

        public List<Sstring> a = new List<Sstring>();
        public List<Sstring> b = new List<Sstring>();
        public List<Sstring> c = new List<Sstring>();

        public RPrediction()
        {
            Serializer.Deserialize(a, "A.xml");
            Serializer.Deserialize(b, "B.xml");
            Serializer.Deserialize(c, "C.xml");
        }

        /// <summary>
        /// riempe la 'list' con gli elementi calcolati da 'words'
        /// </summary>
        public void Fillwordslist()
        {
            ///per ogni parola 
            foreach(string s in words)
            {
                string letter = "";
                ///prendo ogni combinazione di lettere
                for (int i = 0; i < s.Length-1; i++)
                {
                    letter = letter+s.ElementAt(i).ToString();
                    ///se la combinazione non appartiene alla list
                    if (!list.Exists(x => x.Text.Equals(letter)))
                    {
                        ///aggiungo alla lista un elemento Sstring che contiene la combinazione delle prime tot lettere e la lettara successiva
                        list.Add(new Sstring(letter, s.ElementAt(i+1).ToString()));
                    }
                    else
                    {
                        ///altrimenti aggiungo solo la lettera successiva come occorrenza all'elemento Sstring già esistente
                        list.Find(x => x.Text.Equals(letter)).Addchar(s.ElementAt(i + 1).ToString());
                    }
                }
            }

            
        }

        /// <summary>
        /// riempe la lista di words partento da un testo.txt contenente tutte le parole
        /// </summary>
        public void Fillwords(string listwords)
        {
           words = listwords.ToLower().Split(' ').Distinct().ToList();
           List<string> cleanword = new List<string>();
           foreach(string s in words)
           {
               cleanword.Add(Regex.Replace(s, @"\n[0-9]{1,}\t", ""));
            }
            words = cleanword;
        }

        public List<String> FindNextString(string s, int n)
        {
            List<string> slist = new List<string>();

            if (s != null)
            {
                char chars = s[0];

                switch (chars)
                {
                    case 'a':
                        list = a;
                        break;
                    case 'b':
                        list = b;
                        break;
                    case 'c':
                        list = c;
                        break;
                }


                Sstring sstring = list.Find(x => x.Text.Equals(s));

                try
                {
                    if (sstring.Nextchars.Count < n)
                    {
                        n = sstring.Nextchars.Count;
                    }

                    for (int c = 0; c < n; c++)
                    {
                        slist.Add(s + sstring.nextchars[c].Text);
                    }
                }
                catch
                {
                    throw new EndOfWordException(s);
                }
            }
            return slist;
        }

        public void OrderList()
        {
            foreach(Sstring s in list)
            {
                s.nextchars = s.nextchars.OrderByDescending(o => o.Freq).ToList();
            }
        }
    }
}
