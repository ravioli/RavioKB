﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RavioKB.Utilities
{
    public static class Serializer
    {
        /// <summary>
        /// Serializza la lista di Sstring
        /// </summary>
        /// <param name="list"></param>
        /// <param name="fileName"></param>
        public static void SerializeList(List<Sstring> list, string fileName)
        {
            var serializer = new XmlSerializer(typeof(List<Sstring>));
            using (var stream = File.OpenWrite(fileName))
            {
                serializer.Serialize(stream, list);
            }
        }

        /// <summary>
        /// Deserializza la lista di Sstring
        /// </summary>
        /// <param name="list"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static List<Sstring> Deserialize(this List<Sstring> list, string fileName)
        {
            var serializer = new XmlSerializer(typeof(List<Sstring>));
            using (var stream = File.OpenRead(fileName))
            {
                var other = (List<Sstring>)(serializer.Deserialize(stream));
                list.Clear();
                list.AddRange(other);
            }

            return list;
        }
    }
}
