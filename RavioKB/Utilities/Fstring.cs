﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RavioKB.Utilities
{
    /// <summary>
    /// Contiene una lettera e la relativa occorrenza
    /// </summary>
    public class Fstring
    {
        private string text;
        private int freq;

        public Fstring()
        {
        }

        public Fstring(string text, int freq)
        {
            this.text = text;
            this.freq = freq;
        }

        public string Text { get => text; set => text = value; }
        public int Freq { get => freq; set => freq = value; }
    }
}
