﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RavioKB.Utilities
{
    public class SerializeButton
    {
        private string digit;
        private string shiftdigit;
        private string altdigit;
        private string shiftaltdigit;
        private int top;
        private int left;
        private RavioButton.ButtonType buttontype;
        private RavioButton.TextAlign textalign;

        public string Shiftaltdigit { get => shiftaltdigit; set => shiftaltdigit = value; }
        public string Altdigit { get => altdigit; set => altdigit = value; }
        public string Shiftdigit { get => shiftdigit; set => shiftdigit = value; }
        public string Digit { get => digit; set => digit = value; }
        public int Top { get => top; set => top = value; }
        public int Left { get => left; set => left = value; }
        public RavioButton.ButtonType Buttontype { get => buttontype; set => buttontype = value; }
        public RavioButton.TextAlign Textalign { get => textalign; set => textalign = value; }

        public SerializeButton(string digit, string shiftdigit, string altdigit, string shiftaltdigit, int top, int left, RavioButton.ButtonType buttontype, RavioButton.TextAlign textalign)
        {
            Digit = digit;
            Shiftdigit = shiftdigit;
            Altdigit = altdigit;
            Shiftaltdigit = shiftaltdigit;
            Top = top;
            Left = left;
            Buttontype = buttontype;
            Textalign = textalign;
        }
    }
}
