﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RavioKB.Utilities
{
    /// <summary>
    /// contiene una stringa e la lista delle possibili lettere successivo con le relative occorrenze
    /// </summary>
    public class Sstring
    {
        private string text;
        public List<Fstring> nextchars = new List<Fstring>();

        public Sstring()
        {
        }

        public Sstring(string text, string nextchar)
        {
            this.text = text;
            Addchar(nextchar);
        }


        public string Text { get => text; set => text = value; }
        public List<Fstring> Nextchars { get => nextchars; set => nextchars = value; }

        public void Addchar(string text)
        {
            if (!Nextchars.Exists(x => x.Text.Equals(text)))
            {
                Nextchars.Add(new Fstring(text,1));
            }
            else
            {
                Nextchars.Find(x => x.Text.Equals(text)).Freq += 1;
            }
           
        }
    }
}
