﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RavioKB.Utilities
{
    public class Config
    {
        public int textdimension;
        public Color textcolor;
        public List<string> vocal;
        public List<string> cons;
        public int deltaT1;
        public int deltaT2;
        public Color deltaT1color;
        public Color enterColor;
        public Font font;
        public string theme;
        public bool isBlurred;
        public Color blurColor;
        public int transparencePercentage;
        public Color layerColor;
        public Color edgeColor;
        public bool isZeroClick;
        //public Color test = Color.FromArgb(10,20,30);
        public int TransparencePercentage
        {
            get
            {
                return transparencePercentage;
            }
            set
            {
                if (value < 0)
                    transparencePercentage = 0;
                else if (value > 100)
                    transparencePercentage = 100;
                else
                    transparencePercentage = value;
            }
        }


        public Config(int textdimension, Color textcolor, List<string> vocal, List<string> cons, int deltaT1, int deltaT2, Color deltaT1color, Color enterColor, Font font, string theme, bool isBlurred, Color blurColor, int transparencePercentage, Color layerColor, Color edgeColor, bool isZeroClick)
        {
            this.textdimension = textdimension;
            this.textcolor = textcolor;
            this.vocal = vocal;
            this.cons = cons;
            this.deltaT1 = deltaT1;
            this.deltaT2 = deltaT2;
            this.deltaT1color = deltaT1color;
            this.enterColor = enterColor;
            this.font = font;
            this.theme = theme;
            this.isBlurred = isBlurred;
            this.blurColor = blurColor;
            TransparencePercentage = transparencePercentage;
            this.layerColor = layerColor;
            this.edgeColor = edgeColor;
            this.isZeroClick = isZeroClick;
        }

        public Config()
        {
        }
    }
}
