﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RavioKB.Utilities
{
    public static class ColorManager
    {
        /// <summary>
        /// Colora l'immagine di un determinato colore
        /// </summary>
        /// <param name="image">Immagine di partenza</param>
        /// <param name="color">Colore</param>
        /// <returns></returns>
        public static Image ColorImage(Image image, Color color)
        {
            Bitmap result = new Bitmap(image);
            for (int row = 0; row < result.Size.Width; row++)
            {
                for (int col = 0; col < result.Size.Height; col++)
                {
                    Color pixel = result.GetPixel(row, col);
                    Color newPixel = MultiplyColor(pixel, color);
                    result.SetPixel(row, col, newPixel);
                }
            }
            return result;
        }
        /// <summary>
        /// Colora una matrice di immagini
        /// </summary>
        /// <param name="images">Matrice originale</param>
        /// <param name="color">Colore</param>
        /// <returns></returns>
        public static Image[,] ColorImages(Image[,] images, Color color)
        {
            Image[,] newImages = new Image[images.GetLength(0),images.GetLength(1)];
            for (int i = 0; i < images.GetLength(0); i++)
            {
                for (int j = 0; j < images.GetLength(1); j++)
                {
                    newImages[i, j] = ColorImage(images[i, j], color);
                }
            }
            return newImages;
        }

        /// <summary>
        /// Ritorna il prodotto di due colori (normalizzato)
        /// </summary>
        /// <param name="colorA">Colore numero 1</param>
        /// <param name="colorB">Colore numero 2</param>
        /// <returns>Nuovo colore A x B</returns>
        public static Color MultiplyColor(Color colorA, Color colorB)
        {
            int alpha = colorA.A * colorB.A / 255 <= 255 ? colorA.A * colorB.A / 255 : 255;
            int red = colorA.R * colorB.R / 255 <= 255 ? colorA.R * colorB.R / 255 : 255;
            int blue = colorA.B * colorB.B / 255 <= 255 ? colorA.B * colorB.B / 255 : 255;
            int green = colorA.G * colorB.G / 255 <= 255 ? colorA.G * colorB.G / 255 : 255;
            return Color.FromArgb(alpha, red, green, blue);
        }

        /// <summary>
        /// Inverte il colore
        /// </summary>
        /// <param name="color">Colore originale</param>
        /// <returns></returns>
        public static Color InvertColor(Color color)
        {
            return Color.FromArgb(color.A, 255 - color.R, 255 - color.G, 255 - color.B);
        }

    }
}
