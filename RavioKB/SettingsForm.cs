﻿using RavioKB.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RavioKB
{
    public partial class SettingsForm : Form
    {
        Form keyboard;

        public SettingsForm()
        {
            InitializeComponent();
        }

        public SettingsForm(Form keyboard) : this()
        {
            this.keyboard = keyboard;
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            numericUpDown1.Value = Utilities.ConfigManager.config.deltaT1;
            checkBox1.Checked = Utilities.ConfigManager.config.isZeroClick;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Utilities.ConfigManager.config.deltaT1 = (int)numericUpDown1.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Utilities.ConfigManager.WriteFile(Utilities.ConfigManager.Serialize(),"config");
            keyboard.Invalidate();
            Close();

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked)
            {
                Utilities.ConfigManager.config.isZeroClick = true;
                panel1.Enabled = true;
            }
            else
            {
                Utilities.ConfigManager.config.isZeroClick = false;
                panel1.Enabled = false;
            }
        }

        
    }
}
