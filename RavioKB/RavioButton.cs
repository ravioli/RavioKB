﻿using RavioKB.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using RavioKB.Utilities;
//using System.Timers;

namespace RavioKB
{
    public class RavioButton : Control
    {
        #region Campi statici
        public static int defaultDimension = 64;
        /// <summary>
        /// Matrice di immagini per i tasti [n x 2]:
        /// Colonna 1 -> Pulsante a riposo
        /// Colonna 2 -> Pulsante premuto
        /// </summary>
        private static Image[,] DefaultImages = new Image[,]
        {
                { Resources.Button_Default, Resources.Button_Default_Pressed},
                { Resources.Button_OneHalf, Resources.Button_OneHalf_Pressed},
                { Resources.Button_Double, Resources.Button_Double_Pressed},
                { Resources.Button_Enter_1, Resources.Button_Enter_1_Pressed },
                { Resources.Button_Space, Resources.Button_Space_Pressed },
        };
        private static Image[,] Images = new Image[,]
        {
                { Resources.Button_Default, Resources.Button_Default_Pressed},
                { Resources.Button_OneHalf, Resources.Button_OneHalf_Pressed},
                { Resources.Button_Double, Resources.Button_Double_Pressed},
                { Resources.Button_Enter_1, Resources.Button_Enter_1_Pressed },
                { Resources.Button_Space, Resources.Button_Space_Pressed },
        };

        private static Image[,] LayerImages = ColorManager.ColorImages(DefaultImages, Layer.layerColor);
        /// <summary>
        /// Matrice di immagini colorate
        /// </summary>
        private static Image[,] ColoredImages = Utilities.ColorManager.ColorImages(Images, ConfigManager.config.enterColor);

        /// <summary>
        /// Lista di simboli di comandi
        /// </summary>
        private static CommandMap[] Commands =
        {
            new CommandMap("Ctrl","Ctrl"),
            new CommandMap("Alt","Alt"),
            new CommandMap("AltGr","AltGr"),
            new CommandMap("Shift", "\u21E7"), //Shift
            new CommandMap("CapsLock","\u21EA"), //CapsLock
            new CommandMap("\t","\u21E5"), //Tab
            new CommandMap("\b","\u2190"), //Backspace
            new CommandMap("\r","\u21A9"), //Enter
            new CommandMap("TopMostLockTrue", "\u2691"), //Blocco tastiera primo piano attivo
            new CommandMap("TopMostLockFalse", "\u2690"), //Blocco tastiera primo piano inattivo
            new CommandMap("Settings","\u2699"),

        };
        /// <summary>
        /// Comando -> Testo Visualizzato sul pulsante
        /// </summary>
        public struct CommandMap
        {
            public string command;
            public string text;

            public CommandMap(string command, string text)
            {
                this.command = command;
                this.text = text;
            }
            public override bool Equals(object obj)
            {
                if (obj is CommandMap)
                    if (((CommandMap)obj).command == this.command)
                        return true;
                    else
                        return false;
                else if (obj is string)
                    if ((string)obj == command)
                        return true;
                    else
                        return false;
                else
                    return false;
            }
        }
        /// <summary>
        /// Tipo di pulsante (Default, Spazio ....)
        /// </summary>
        public enum ButtonType
        {
            Default = 0,
            OneHalf = 1,
            Double = 2,
            Enter = 3,
            Space = 4,
        };
        /// <summary>
        /// Allineamento del testo nel pulsante
        /// </summary>
        public enum TextAlign
        {
            Left = 0,
            Center = 1,
            Right = 2
        }
        /// <summary>
        /// Timer 1 per delta1
        /// </summary>


        public static Font defaultFont = ConfigManager.config.font;

        private static bool initialized = false;


        #endregion

        #region Metodi statici
        /// <summary>
        /// Formatta il testo in modo particolare per alcuni caratteri speciali
        /// </summary>
        /// <param name="text">Stringa da scrivere</param>
        /// <returns></returns>
        public static string SpecialKeyFormatter(String text)
        {
            if (text == "+" || text == "^" || text == "%" || text == "(" || text == ")" || text == "{" || text == "}")
            {
                return "{" + text + "}";
            }
            else
                return text;
        }

        #endregion

        #region Campi privati

        /// <summary>
        /// Campo per tipo pulsante
        /// </summary>
        private ButtonType type;
        /// <summary>
        /// Campo per allineamento del testo
        /// </summary>
        private TextAlign alignment;
        /// <summary>
        /// Campo per stato pulsante
        /// </summary>
        private bool isPressed;
        /// <summary>
        /// Campo che dice se il mouse sia sopra o no
        /// </summary>
        private bool isMouseIn;
        /// <summary>
        /// Stringa di default
        /// </summary>
        private string defaultKey;
        /// <summary>
        /// Stringa in shift mode
        /// </summary>
        private string shiftKey;
        /// <summary>
        /// Stringa in altGr gr mode
        /// </summary>
        private string altKey;
        /// <summary>
        /// Stringa in shift+altGr gr mode
        /// </summary>
        private string shiftAltKey;
        /// <summary>
        /// Immagine normale
        /// </summary>
        private Image img;
        /// <summary>
        /// Immagine premuta
        /// </summary>
        private Image imgPressed;
        /// <summary>
        /// Immagine layer
        /// </summary>
        private Image layerImg;
        /// <summary>
        /// Immagine layer premuta
        /// </summary>
        private Image layerImgPressed;
        /// <summary>
        /// Immagine normale default
        /// </summary>
        private Image defaultImg;
        /// <summary>
        /// Immagine premuta default
        /// </summary>
        private Image defaultImgPressed;
        /// <summary>
        /// Parametro che modifica il puntatore all'immagine da visualizzare
        /// </summary>
        /// 
        private bool isImgModified;
        /// <summary>
        /// Immagine normale modificata
        /// </summary>
        private Image imgM;
        /// <summary>
        /// Immagine premuta modificata
        /// </summary>
        private Image imgPressedM;
        /// <summary>
        /// Timer 1 per delta1
        /// </summary>
        private Timer timer1;
        /// <summary>
        /// Timer 2 per delta2
        /// </summary>
        private Timer timer2;



        #region In config

        /// <summary>
        /// Tempo primo timer
        /// </summary>
        private int deltaT1;
        /// <summary>
        /// Tempo secondo timer
        /// </summary>
        private int deltaT2;


        private Color activateColor;
        private Color selectionColor;
        private Color borderColor;
        #endregion

        #endregion

        #region Proprietà

        /// <summary>
        /// Tipo di pulsante
        /// </summary>
        public ButtonType Type
        {
            get => type;
            set
            {
                type = value;
                SetParams();
                OnTypeChanged(new EventArgs());
            }
        }
        /// <summary>
        /// Stato pulsante
        /// </summary>
        public bool IsPressed
        {
            get => isPressed;
        }
        /// <summary>
        /// Immagine da disegnare
        /// </summary>
        public Image Image
        {
            get
            {
                Image img=this.img;
                Image imgPressed=this.imgPressed;
                if (Parent != null)
                    if (Parent is Layer)
                        if ((Parent as Layer).Index == 0)
                            if (!IsPressable)
                            {
                                img = this.imgPressed;
                                imgPressed = this.imgPressed;

                            }
                            else if (!isImgModified)
                            {
                                img = this.img;
                                imgPressed = this.imgPressed;
                            }
                            else
                            {
                                img = this.imgM;
                                imgPressed = this.imgPressedM;
                            }
                        else
                        {
                            if (!IsPressable)
                            {
                                img = this.layerImgPressed;
                                imgPressed = this.layerImgPressed;

                            }
                            else if (!isImgModified)
                            {
                                img = this.layerImg;
                                imgPressed = this.layerImgPressed;
                            }
                            else
                            {
                                img = this.imgM;
                                imgPressed = this.imgPressedM;
                            }
                        }
                switch (DefaultKey)
                {
                    case "Ctrl":
                        if (Parent != null)
                            if (Parent is Layer)
                                if ((Parent as Layer).Ctrl)
                                    return imgPressed;
                                else
                                    return img;
                        break;
                    case "Alt":
                    case "AltGr":
                        if (Parent != null)
                            if (Parent is Layer)
                                if ((Parent as Layer).AltGr)
                                    return imgPressed;
                                else
                                    return img;
                        break;
                    case "Shift":
                        if (Parent != null)
                            if (Parent is Layer)
                                if ((Parent as Layer).Shift)
                                    return imgPressed;
                                else
                                    return img;
                        break;
                    case "CapsLock":
                        if (Parent != null)
                            if (Parent is Layer)
                                if ((Parent as Layer).CapsLock)
                                    return imgPressed;
                                else
                                    return img;
                        break;
                    /*case "TopMostLockTrue":
                        return imgPressed;
                    case "TopMostLockFalse":
                        return img;*/

                    default:
                        if (!IsPressed)
                            return img;
                        else
                            return imgPressed;
                }
                return img;

            }
        }
        /// <summary>
        /// Allineamento del testo
        /// </summary>
        public TextAlign Alignment { get => alignment; set => alignment = value; }
        /// <summary>
        /// Stringa di default
        /// </summary>
        public string DefaultKey { get => defaultKey; }
        /// <summary>
        /// Stringa in shift mode
        /// </summary>
        public string ShiftKey { get => shiftKey; }
        /// <summary>
        /// Stringa in altGr gr mode
        /// </summary>
        public string AltKey { get => altKey; }
        /// <summary>
        /// Stringa in shift+altGr gr mode
        /// </summary>
        public string ShiftAltKey { get => shiftAltKey; }
        /// <summary>
        /// Mouse sopra o no
        /// </summary>
        public bool IsMouseIn { get => isMouseIn; set => isMouseIn = value; }

        public bool IsPressable
        {
            get
            {
                if (defaultKey == "" && shiftKey == "" && altKey == "" && shiftAltKey == "")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }



        #endregion

        #region Costruttori

        /// <summary>
        /// Costruttore di Default
        /// </summary>
        public RavioButton()
        {
            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            Type = ButtonType.Default;
            Alignment = TextAlign.Center;
            isPressed = false;
            Margin = new Padding(0);

            Font = defaultFont;
            SetParams();
            switch (Keyboard.theme)
            {
                case "Dark":
                    ForeColor = Utilities.ColorManager.InvertColor(Color.FromArgb(255, 80, 80, 80));
                    BackColor = Utilities.ColorManager.InvertColor(SystemColors.ControlDark);
                    if (!initialized)
                    {
                        //ColoredImages = Utilities.ColorManager.ColorImages(Images, ColorManager.InvertColor(ConfigManager.config.deltaT3color));
                        Images = Utilities.ColorManager.ColorImages(Images, Color.FromArgb(255, 80, 80, 80));
                        initialized = true;
                    }

                    break;
                default:
                    ForeColor = SystemColors.ControlDarkDark;
                    BackColor = SystemColors.ControlDark;
                    break;
            }
            isImgModified = false;

            timer1 = new Timer();
            timer2 = new Timer();

            timer1.Tick += OnDelta1Elapsed;
            timer2.Tick += OnTick;
            timer2.Interval = ConfigManager.config.deltaT2;

        }






        /// <summary>
        /// Costruttore con testo:
        /// </summary>
        /// <param name="text">Testo visualizzato</param>
        public RavioButton(string text) : this()
        {
            Text = text;
        }
        /// <summary>
        ///Costruttore con testo e genitore:
        /// </summary>
        /// <param name="parent">Controllo genitore</param>
        /// <param name="text">Testo visualizzato</param>
        public RavioButton(Control parent, string text) : this(text)
        {
            Parent = parent;
        }
        /// <summary>
        ///Costruttore con testo, posizione e dimensione:
        /// </summary>
        /// <param name="text">Testo visualizzato</param>
        /// <param name="left">Posizione X</param>
        /// <param name="top">Posizione Y</param>
        /// <param name="width">Larghezza</param>
        /// <param name="height">Altezza</param>
        public RavioButton(string text, int left, int top, int width, int height) : this(text)
        {
            Left = left;
            Top = top;
            Width = width;
            Height = height;
        }
        /// <summary>
        ///Costruttore con genitore, testo, posizione e dimensione:
        /// </summary>
        /// <param name="parent">Controllo genitore</param>
        /// <param name="text">Testo visualizzato</param>
        /// <param name="left">Posizione X</param>
        /// <param name="top">Posizione Y</param>
        /// <param name="width">Larghezza</param>
        /// <param name="height">Altezza</param>
        public RavioButton(Control parent, string text, int left, int top, int width, int height) : this(text, left, top, width, height)
        {
            Parent = parent;
        }
        /// <summary>
        /// Costruttore con keyInfo:
        /// Descrive il pulsante a partire dalle informazioni del carattere
        /// </summary>
        /// <param name="defaultKey">Stringa di default del tasto</param>
        /// <param name="shiftKey">Stringa in shift mode</param>
        /// <param name="altKey">Stringa in altGr gr mode</param>
        /// <param name="shiftAltKey">Stringa in shift + altGr gr mode</param>
        public RavioButton(string defaultKey, string shiftKey, string altKey, string shiftAltKey) : this()
        {
            this.defaultKey = defaultKey;
            this.shiftKey = shiftKey;
            this.altKey = altKey;
            this.shiftAltKey = shiftAltKey;
            Text = ToString();
        }
        /// <summary>
        /// Costruttore con keyInfo:
        /// Descrive il pulsante a partire dalle informazioni del carattere
        /// </summary>
        /// <param name="parent">Controllo genitore</param>
        /// <param name="defaultKey">Stringa di default del tasto</param>
        /// <param name="shiftKey">Stringa in shift mode</param>
        /// <param name="altKey">Stringa in altGr gr mode</param>
        /// <param name="shiftAltKey">Stringa in shift + altGr gr mode</param>
        public RavioButton(Control parent, string defaultKey, string shiftKey, string altKey, string shiftAltKey) : this(defaultKey, shiftKey, altKey, shiftAltKey)
        {
            Parent = parent;
        }
        /// <summary>
        /// Costruttore con keyInfo:
        /// Descrive il pulsante a partire dalle informazioni del carattere
        /// </summary>
        /// <param name="defaultKey">Stringa di default del tasto</param>
        /// <param name="shiftKey">Stringa in shift mode</param>
        /// <param name="altKey">Stringa in altGr gr mode</param>
        /// <param name="shiftAltKey">Stringa in shift + altGr gr mode</param>
        /// <param name="left">Posizione X</param>
        /// <param name="top">Posizione Y</param>
        /// <param name="width">Larghezza</param>
        /// <param name="height">Altezza</param>
        public RavioButton(string defaultKey, string shiftKey, string altKey, string shiftAltKey, int left, int top, int width, int height) : this(defaultKey, shiftKey, altKey, shiftAltKey)
        {
            Left = left;
            Top = top;
            Width = width;
            Height = height;
        }
        /// <summary>
        /// Costruttore con keyInfo:
        /// Descrive il pulsante a partire dalle informazioni del carattere
        /// </summary>
        /// <param name="parent">Controllo genitore</param>
        /// <param name="defaultKey">Stringa di default del tasto</param>
        /// <param name="shiftKey">Stringa in shift mode</param>
        /// <param name="altKey">Stringa in altGr gr mode</param>
        /// <param name="shiftAltKey">Stringa in shift + altGr gr mode</param>
        /// <param name="left">Posizione X</param>
        /// <param name="top">Posizione Y</param>
        /// <param name="width">Larghezza</param>
        /// <param name="height">Altezza</param>
        public RavioButton(Control parent, string defaultKey, string shiftKey, string altKey, string shiftAltKey, int left, int top, int width, int height) : this(defaultKey, shiftKey, altKey, shiftAltKey, left, top, width, height)
        {
            Parent = parent;
        }
        /// <summary>
        /// Costruttore con testo e tipo:
        /// -- non setta il keyInfo quindi va fatto a parte
        /// Oppure usare il costruttore con keyInfo
        /// </summary>
        /// <param name="text">Testo visualizzato</param>
        /// <param name="type">Tipo del pulsante (onehalf,double)</param>
        /// <param name="alignment">Allinemento del testo</param>
        public RavioButton(string text, ButtonType type, TextAlign alignment = TextAlign.Center) : this(text)
        {
            Type = type;
            Alignment = alignment;
        }
        /// <summary>
        ///Costruttore con testo  tipo e genitore:
        /// -- non setta il keyInfo quindi va fatto a parte
        /// Oppure usare il costruttore con keyInfo
        /// </summary>
        /// <param name="parent">Controllo genitore</param>
        /// <param name="text">Testo visualizzato</param>
        /// <param name="type">Tipo pulsante</param>
        /// <param name="alignment">Allinemento del testo</param>
        public RavioButton(Control parent, string text, ButtonType type, TextAlign alignment = TextAlign.Center) : this(parent, text)
        {
            Type = type;
            Alignment = alignment;
        }
        /// <summary>
        ///Costruttore con testo, posizione e tipo:
        /// -- non setta il keyInfo quindi va fatto a parte
        /// Oppure usare il costruttore con keyInfo
        /// </summary>
        /// <param name="text">Testo visualizzato</param>
        /// <param name="left">Posizione X</param>
        /// <param name="top">Posizione Y</param>
        /// <param name="type">Tipo pulsante</param>
        /// <param name="alignment">Allinemento del testo</param>
        public RavioButton(string text, int left, int top, ButtonType type, TextAlign alignment = TextAlign.Center) : this(text, left, top, 0, 0)
        {
            Type = type;
            Alignment = alignment;
        }
        /// <summary>
        ///Costruttore con genitore, testo, posizione e tipo:
        /// -- non setta il keyInfo quindi va fatto a parte
        /// Oppure usare il costruttore con keyInfo
        /// </summary>
        /// <param name="parent">Controllo genitore</param>
        /// <param name="text">Testo visualizzato</param>
        /// <param name="left">Posizione X</param>
        /// <param name="top">Posizione Y</param>
        /// <param name="type">Tipo pulsante</param>
        /// <param name="alignment">Allinemento del testo</param>
        public RavioButton(Control parent, string text, int left, int top, ButtonType type, TextAlign alignment = TextAlign.Center) : this(parent, text, left, top, 0, 0)
        {
            Type = type;
            Alignment = alignment;
        }
        /// <summary>
        /// Costruttore con keyInfo e tipo:
        /// Descrive il pulsante a partire dalle informazioni del carattere
        /// </summary>
        /// <param name="defaultKey">Stringa di default del tasto</param>
        /// <param name="shiftKey">Stringa in shift mode</param>
        /// <param name="altKey">Stringa in altGr gr mode</param>
        /// <param name="shiftAltKey">Stringa in shift + altGr gr mode</param>
        /// <param name="type">Tipo pulsante</param>
        /// <param name="alignment">Allinemento del testo</param>
        public RavioButton(string defaultKey, string shiftKey, string altKey, string shiftAltKey, ButtonType type, TextAlign alignment = TextAlign.Center) : this(defaultKey, shiftKey, altKey, shiftAltKey)
        {
            Type = type;
            Alignment = alignment;
        }
        /// <summary>
        /// Costruttore con keyInfo, genitore e tipo:
        /// Descrive il pulsante a partire dalle informazioni del carattere
        /// </summary>
        /// <param name="parent">Controllo genitore</param>
        /// <param name="defaultKey">Stringa di default del tasto</param>
        /// <param name="shiftKey">Stringa in shift mode</param>
        /// <param name="altKey">Stringa in altGr gr mode</param>
        /// <param name="shiftAltKey">Stringa in shift + altGr gr mode</param>
        /// <param name="type">Tipo pulsante</param>
        /// <param name="alignment">Allinemento del testo</param>
        public RavioButton(Control parent, string defaultKey, string shiftKey, string altKey, string shiftAltKey, ButtonType type, TextAlign alignment = TextAlign.Center) : this(parent, defaultKey, shiftKey, altKey, shiftAltKey)
        {
            Type = type;
            Alignment = alignment;
        }
        /// <summary>
        /// Costruttore con keyInfo, posizione e tipo:
        /// Descrive il pulsante a partire dalle informazioni del carattere
        /// </summary>
        /// <param name="defaultKey">Stringa di default del tasto</param>
        /// <param name="shiftKey">Stringa in shift mode</param>
        /// <param name="altKey">Stringa in altGr gr mode</param>
        /// <param name="shiftAltKey">Stringa in shift + altGr gr mode</param>
        /// <param name="left">Posizione X</param>
        /// <param name="top">Posizione Y</param>
        /// <param name="type">Tipo pulsante</param>
        /// <param name="alignment">Allinemento del testo</param>
        public RavioButton(string defaultKey, string shiftKey, string altKey, string shiftAltKey, int left, int top, ButtonType type, TextAlign alignment = TextAlign.Center) : this(defaultKey, shiftKey, altKey, shiftAltKey, left, top, 0, 0)
        {
            Type = type;
            Alignment = alignment;
        }
        /// <summary>
        /// Costruttore con keyInfo, genitore, posizione e tipo:
        /// Descrive il pulsante a partire dalle informazioni del carattere
        /// </summary>
        /// <param name="parent">Controllo genitore</param>
        /// <param name="defaultKey">Stringa di default del tasto</param>
        /// <param name="shiftKey">Stringa in shift mode</param>
        /// <param name="altKey">Stringa in altGr gr mode</param>
        /// <param name="shiftAltKey">Stringa in shift + altGr gr mode</param>
        /// <param name="left">Posizione X</param>
        /// <param name="top">Posizione Y</param>
        /// <param name="type">Tipo pulsante</param>
        /// <param name="alignment">Allinemento del testo</param>
        public RavioButton(Control parent, string defaultKey, string shiftKey, string altKey, string shiftAltKey, int left, int top, ButtonType type, TextAlign alignment = TextAlign.Center) : this(parent, defaultKey, shiftKey, altKey, shiftAltKey, left, top, 0, 0)
        {
            Type = type;
            Alignment = alignment;

        }

        #endregion

        #region Metodi


        /// <summary>
        /// Setta i parametri per tipo pulsante (es. dimensioni, colori...)
        /// ToBeImplemented
        /// </summary>
        public void SetParams()
        {
            switch (Type)
            {
                case ButtonType.OneHalf:
                    Size = new Size((int)(defaultDimension * 1.5f), defaultDimension);
                    defaultImg = DefaultImages[1, 0];
                    defaultImgPressed = DefaultImages[1, 1];
                    layerImg = LayerImages[1, 0];
                    layerImgPressed = LayerImages[1, 1];
                    img = Images[1, 0];
                    imgPressed = Images[1, 1];
                    break;
                case ButtonType.Double:
                    Size = new Size((int)(defaultDimension * 2f), defaultDimension);
                    defaultImg = DefaultImages[2, 0];
                    defaultImgPressed = DefaultImages[2, 1];
                    layerImg = LayerImages[2, 0];
                    layerImgPressed = LayerImages[2, 1];
                    img = Images[2, 0];
                    imgPressed = Images[2, 1];
                    break;
                case ButtonType.Enter:
                    Size = new Size((int)(defaultDimension * 1.5f), (int)(defaultDimension * 2f));
                    defaultImg = DefaultImages[3, 0];
                    defaultImgPressed = DefaultImages[3, 1];
                    layerImg = LayerImages[3, 0];
                    layerImgPressed = LayerImages[3, 1];
                    img = Images[3, 0];
                    imgPressed = Images[3, 1];
                    break;
                case ButtonType.Space:
                    Size = new Size((int)(defaultDimension * 5f), defaultDimension);
                    defaultImg = DefaultImages[4, 0];
                    defaultImgPressed = DefaultImages[4, 1];
                    layerImg = LayerImages[4, 0];
                    layerImgPressed = LayerImages[4, 1];
                    img = Images[4, 0];
                    imgPressed = Images[4, 1];
                    break;
                default:
                    Size = new Size(defaultDimension, defaultDimension);
                    defaultImg = DefaultImages[0, 0];
                    defaultImgPressed = DefaultImages[0, 1];
                    layerImg = LayerImages[0, 0];
                    layerImgPressed = LayerImages[0, 1];
                    img = Images[0, 0];
                    imgPressed = Images[0, 1];
                    break;
            }

        }
        /// <summary>
        /// Maiuscolo o minuscolo a seconda del valore di "Shift" della key
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string key = DefaultKey;
            ///Se il pulsante è un comando, guardo lookup
            if (Commands.Contains(new CommandMap(DefaultKey, string.Empty)))
            {
                return Array.Find<CommandMap>(Commands, x => x.Equals(DefaultKey)).text;
            }
            if (Parent is Layer)
            {
                if ((Parent as Layer).Shift && (Parent as Layer).AltGr && ShiftAltKey != "")
                {
                    key = ShiftAltKey;
                }
                else if ((Parent as Layer).Shift && ShiftKey != "")
                {
                    key = ShiftKey;
                }
                else if ((Parent as Layer).AltGr && AltKey != "")
                {
                    key = AltKey;
                }
                if ((Parent as Layer).CapsLock)
                {
                    string c = Command();
                    if (c == "" || c == null)
                    {
                        return key.ToUpper();
                    }
                }
            }
            return key;
        }
        /// <summary>
        /// Ritorna il carattere da inserire
        /// </summary>
        /// <returns></returns>
        public string ToKey()
        {
            string key = "";

            switch (Command())
            {
                ///Shift
                case "Shift":
                    if (Parent != null)
                        if (Parent is Layer)
                            (Parent as Layer).Shift = !(Parent as Layer).Shift;
                    return key;
                ///Caps
                case "CapsLock":
                    if (Parent != null)
                        if (Parent is Layer)
                            (Parent as Layer).CapsLock = !(Parent as Layer).CapsLock;
                    return key;
                ///AltGr
                case "AltGr":
                    if (Parent != null)
                        if (Parent is Layer)
                            (Parent as Layer).AltGr = !(Parent as Layer).AltGr;
                    return key;
                ///Ctrl
                case "Ctrl":
                    if (Parent != null)
                        if (Parent is Layer)
                            (Parent as Layer).Ctrl = !(Parent as Layer).Ctrl;
                    return key;
                case "\b":
                case "\r":
                case "\t":
                    return DefaultKey;
                ///Disabilta primo piano
                case "TopMostLockTrue":
                    if (Parent != null)
                        if (Parent is Layer)
                            if (Parent.Parent is Keyboard)
                                if ((Parent.Parent as Keyboard).TopMost == true)
                                {
                                    (Parent.Parent as Keyboard).TopMost = false;
                                    this.defaultKey = "TopMostLockFalse";
                                    UpdateText();
                                }
                    return key;

                case "TopMostLockFalse":
                    if (Parent != null)
                        if (Parent is Layer)
                            if (Parent.Parent is Keyboard)
                                if ((Parent.Parent as Keyboard).TopMost == false)
                                {
                                    (Parent.Parent as Keyboard).TopMost = true;
                                    this.defaultKey = "TopMostLockTrue";
                                    UpdateText();
                                }
                    return key;

                /// Caratteri speciali SendKey
                case "Settings":
                    SettingsForm settings = new SettingsForm(Parent.Parent as Keyboard);
                    settings.Show();
                    settings.Activate();
                    
                    return key;
                    
                default:
                    if (Parent != null)
                        if (Parent is Layer)
                        {
                            key = Text;
                        }
                    break;
            }

            key = "";

            if (Keyboard.prevbutton != null)
            {
                if (Keyboard.prevbutton.Text == Text)
                {
                    Keyboard.prevbutton = null;
                    key = Text;
                    if (Parent != null)
                        (Parent as Layer).DisposeLayers();
                    return key;
                }
                else
                {
                    Keyboard.prevbutton = this;
                }
            }
            else
            {
                Keyboard.prevbutton = this;
            }

            try
            {
                Layer l;
                if (Text != "")
                    if (Char.IsLetter(Text[Text.Length - 1]))
                        l = new Layer(this);
                    else
                        throw new EndOfWordException(Text);
            }
            catch (EndOfWordException e)
            {
                key = Text;

                (Parent as Layer).Shift = false;
                (Parent as Layer).AltGr = false;
                (Parent as Layer).DisposeLayers();
            }


            return SpecialKeyFormatter(key);


        }
        /// <summary>
        /// Aggiorna il testo visualizzato
        /// </summary>
        public void UpdateText()
        {
            string old = Text;
            Text = ToString();
            if (old != Text)
            {
                Invalidate();
                Update();
            }
        }
        /// <summary>
        /// Setta la posizione della stringa nel pulsante
        /// </summary>
        /// <param name="sizeF"></param>
        /// <returns></returns>
        private Point CalculateTextPosition(SizeF sizeF)
        {
            Point position;
            switch (Type)
            {
                case ButtonType.OneHalf:
                case ButtonType.Double:
                    switch (Alignment)
                    {
                        case TextAlign.Left:
                            position = new Point((int)((ClientSize.Width - sizeF.Width) * 0.1f), (int)((this.ClientSize.Height - sizeF.Height) / 2));
                            break;
                        case TextAlign.Right:
                            position = new Point((int)((ClientSize.Width - sizeF.Width) * 0.9f), (int)((this.ClientSize.Height - sizeF.Height) / 2));
                            break;
                        case TextAlign.Center:
                            position = new Point((int)((ClientSize.Width - sizeF.Width) / 2), (int)((this.ClientSize.Height - sizeF.Height) / 2));
                            break;
                        default:
                            position = new Point((int)((ClientSize.Width - sizeF.Width) / 2), (int)((this.ClientSize.Height - sizeF.Height) / 2));
                            break;
                    }
                    break;
                case ButtonType.Enter:
                    position = new Point((int)((ClientSize.Width - sizeF.Width) * 0.9f), (int)((this.ClientSize.Height - sizeF.Height) / 4));
                    break;
                default:
                    position = new Point((int)((ClientSize.Width - sizeF.Width) / 2), (int)((this.ClientSize.Height - sizeF.Height) / 2));
                    break;
            }
            return position;
        }
        /// <summary>
        /// Ritorna il comando corrispondente
        /// </summary>
        /// <returns></returns>
        public string Command()
        {
            string c = "";
            try
            { c = Array.Find(Commands, x => x.Equals(DefaultKey)).command; }
            catch
            { return ""; }
            //Prima di ritornare controllo di essere al layer 0
            //Se no nessun controllo (almeno da poter scrivere altGr o caps lock
            if (Parent != null)
                if (Parent is Layer)
                    if ((Parent as Layer).Index > 0)
                        c = "";
            return c;
        }
        /// <summary>
        /// Funzione che ridimensiona il font
        /// </summary>
        /// <param name="fillingPercentage">Percentuale di riempimento del pulsante</param>
        public void FontResizer(float fillingPercentage)
        {
            Graphics g = CreateGraphics();
            string s = new string('X', Text.Length);
            SizeF textMeasure = g.MeasureString(s, Font);
            if (textMeasure.Width >= fillingPercentage * Size.Width)
            {
                Font = new Font(Font.FontFamily, Font.Size * 0.98f, FontStyle.Bold);
                FontResizer(fillingPercentage);
            }
            else
            {
            }

        }
        /// <summary>
        /// Cambia colore
        /// </summary>
        /// <param name="color"></param>
        public void ChangeColor(Color color)
        {
            img = ColorManager.ColorImage(img, color);
            imgPressed = ColorManager.ColorImage(imgPressed, color);
        }
        /// <summary>
        /// Crea un oggetto serializzabile
        /// </summary>
        /// <returns></returns>
        public SerializeButton ConvertToSerialize()
        {
            return new SerializeButton(defaultKey, shiftKey, altKey, shiftAltKey, Top, Left, type, alignment);
        }
        #endregion

        #region Handler per eventi

        /// <summary>
        /// Handler evento click down:
        /// Mouse premuto-> IsPressed = true
        /// Invalido il controllo
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            isPressed = true;
            Invalidate();
            Update();
            base.OnMouseDown(e);
        }

        /// <summary>
        /// Handler evento click up:
        /// Mouse rilasciato-> IsPressed = false
        /// Valido il controllo (uguale a invalido)
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            isPressed = false;
            Invalidate();
            Update();
            base.OnMouseUp(e);
            ///Tentativo di scrivere in un buffer
            Keyboard.OnButtonClick(this, e);
        }

        /// <summary>
        /// Handler per evento se mouse entra nel controllo
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseEnter(EventArgs e)
        {
            isMouseIn = true;
            imgM = ColoredImages[(int)type, 0];
            imgPressedM = ColoredImages[(int)type, 1];
            isImgModified = true;
            Invalidate();
            Update();
            if (ConfigManager.config.isZeroClick)
            {
                timer1.Interval = ConfigManager.config.deltaT1;
                timer1.Start();
            }
            base.OnMouseEnter(e);
        }

        private void OnDelta1Elapsed(object sender, EventArgs e)
        {
            timer1.Stop();
            switch (Keyboard.theme)
            {
                case "Dark":
                    imgM = Utilities.ColorManager.ColorImage(defaultImg, ConfigManager.config.deltaT1color);
                    imgPressedM = Utilities.ColorManager.ColorImage(defaultImgPressed, ConfigManager.config.deltaT1color);
                    break;
                default:
                    imgM = Utilities.ColorManager.ColorImage(defaultImg, ConfigManager.config.deltaT1color);
                    imgPressedM = Utilities.ColorManager.ColorImage(defaultImgPressed, ConfigManager.config.deltaT1color);
                    break;
            }

            isImgModified = true;
            Invalidate();
            Update();
            isPressed = true;
            timer2.Interval = ConfigManager.config.deltaT2;
            timer2.Start();
        }

        /*private void OnDelta2Elapsed(object sender, EventArgs e)
        {
            timer2.Stop();
            switch (Keyboard.theme)
            {
                case "Dark":
                    imgM = Utilities.ColorManager.ColorImage(defaultImg, ConfigManager.config.deltaT2color);
                    imgPressedM = Utilities.ColorManager.ColorImage(defaultImgPressed, ConfigManager.config.deltaT2color);
                    break;
                default:
                    imgM = Utilities.ColorManager.ColorImage(img, ConfigManager.config.deltaT2color);
                    imgPressedM = Utilities.ColorManager.ColorImage(imgPressed, ConfigManager.config.deltaT2color);
                    break;
            }
            isImgModified = true;
            Invalidate();
            isPressed = true;
            Invalidate();
            timer3.Start();
        }*/

        private void OnTick(object sender, EventArgs e)
        {
            timer2.Stop();
            isPressed = false;
            isImgModified = false;
            Invalidate();
            Update();
            Keyboard.OnButtonClick(this, e);
        }

        /// <summary>
        /// Handler per evento se mouse lascia il controllo
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(EventArgs e)
        {
            isMouseIn = false;
            isImgModified = false;
            isPressed = false;
            Invalidate();
            Update();
            timer1.Stop();
            timer2.Stop();
            /// Riga da eliminare dopo
            //ForeColor = SystemColors.ControlDarkDark;
            ///
            base.OnMouseLeave(e);
        }

        /// <summary>
        /// Handler per draw
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.DrawImage(Image, 0, 0, Size.Width, Size.Height);
            ///Tentativo di disegnare un rettangolo intorno ad un pulsante
            //if (isMouseIn)
            //    e.Graphics.DrawRectangle(Pens.Red, new Rectangle(0, 0, Size.Width - 1, Size.Height - 1));
            //int height = (int)counter.ElapsedMilliseconds / 2000 * defaultDimension;
            //e.Graphics.FillRectangle(new SolidBrush(Color.Orange), new Rectangle(Left, Top, Width, height));
            // Se c'è testo
            if (Text.Length > 0)
            {
                SizeF size = e.Graphics.MeasureString(Text, Font);
                Point position = CalculateTextPosition(size);
                e.Graphics.DrawString(Text, Font,
                    new SolidBrush(ForeColor),
                    position.X,
                    position.Y);
            }
            base.OnPaint(e);
        }

        /// <summary>
        /// Handler per evento di cambiamento testo
        /// </summary>
        /// <param name="e"></param>
        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);
        }

        /// <summary>
        /// Handler per evento di cambiamento tipo
        /// </summary>
        /// <param name="e"></param>
        protected void OnTypeChanged(EventArgs e)
        {
            TypeChanged?.Invoke(this, e);
        }


        #endregion

        #region Eventi
        /// <summary>
        /// Evento TypeChanged
        /// </summary>
        public event EventHandler TypeChanged;

        #endregion

    }
}
