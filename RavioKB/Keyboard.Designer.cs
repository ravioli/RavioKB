﻿using RavioKB.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using System.Diagnostics;

namespace RavioKB
{
    public partial class Keyboard
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        public static RPrediction ravioPrediction;
        public static RavioButton prevbutton = null;
        public Process notepad;
        
        public static string theme = ConfigManager.config.theme;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo bastardo e non necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice sennò bestemmi come un forsennato.
        /// </summary>
        private void InitializeComponent()
        {
            ///Ti piace essere vuoto brutto stronzo?? Muhuhuhahahah
        }

        /// <summary>
        /// InizializeComponent personalizzata che non si autocompleti e autosminchi ogni volta che compili.
        /// Usare questo invece dell'altro.
        /// </summary>
        #endregion
        private void CustomInitializeComponent()
        {
            ravioPrediction = new RPrediction();
            
            this.SuspendLayout();
            /*TextBox textBox = new TextBox();
            textBox.Location = new Point(1, 1); ;
            textBox.Size = new Size(100, 30);
            textBox.Text = ConfigManager.config.deltaT1.ToString();
            textBox.TextAlign = HorizontalAlignment.Center;*/
            
            
            //this.Controls.Add(textBox);
            ///Sezione Keyboard
            switch(theme)
            {
                case "Dark":
                    this.BackColor = Utilities.ColorManager.InvertColor(System.Drawing.SystemColors.ControlDark);
                    break;
                default:
                    this.BackColor = System.Drawing.SystemColors.ControlDark;
                    break;
            }
            
            Layer BaseLayer = new Layer(this);
            
            BaseLayer.Focus();
            //BaseLayer.Location = new Point(0, textBox.Height + 5);
            this.ClientSize = BaseLayer.Size /*+ new Size(0, textBox.Height + 5)*/; //Rimosso per debug
            ///this.ClientSize = new Size(500, 500); //Debug
            this.Icon = Properties.Resources.RavioIcon;
            this.Left = Screen.FromControl(this).WorkingArea.Right - Size.Width;
            this.Top = Screen.FromControl(this).WorkingArea.Bottom - Size.Height;
            this.TopMost = true;
            
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Name = "RavioKB";
            this.Text = "RavioKB";

            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            ///

            this.ResumeLayout(false);

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Keyboard));
            
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            try { notepad.Kill(); }
            catch { };
            base.OnClosed(e);
        }

    }
}