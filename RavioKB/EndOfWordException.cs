﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RavioKB
{
    public class EndOfWordException : Exception
    {
        private string word;

        public EndOfWordException(string word)
        {
            Word = word;
        }

        public string Word { get => word; set => word = value; }
    }
}
