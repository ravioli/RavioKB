﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using RavioKB.Utilities;
using RavioKB.Properties;
using System.Drawing;

namespace RavioKB
{
    static class Program
    {
        
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //RPrediction r = new RPrediction();
            //r.Fillwords(Resources.B);
            //r.Fillwordslist();
            //r.OrderList();
            //Serializer.SerializeList(r.list, "B.xml");

            //Leggo le variabili di configurazione
            // ConfigManager.Serialize();

            
            
            ConfigManager.Deserialize(ConfigManager.ReadFile("config"));
            
            if(ConfigManager.config == null)
            {
                ConfigManager.config = new Config(21, Color.Black, new List<string> { "a", "e", "i", "o", "u" },
                                                  new List<string> { "l", "m", "n", "r", "s" }, 1000, 100, Color.OrangeRed,
                                                  Color.Orange, new Font("Open Sans", 21, FontStyle.Bold), "Light", true,
                                                  Color.Black, 85, Color.Pink, Color.Red, true);
            }




            Process p = Process.Start("notepad");
            Keyboard k = new Keyboard();
            k.notepad = p;
            Application.Run(k);
            
        }
    }
}
