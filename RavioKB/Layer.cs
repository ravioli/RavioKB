﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using static RavioKB.RavioButton;
using RavioKB.Properties;
using RavioKB.Utilities;

namespace RavioKB
{
    public class Layer : Control
    {
        #region Metodi statici

        /// <summary>
        /// Metodo che genera un layer intorno ad un button
        /// </summary>
        /// <param name="ravioButton">Pulsante intorno al quale si costruisce il layer</param>
        /// <returns></returns>
        public static List<RavioButton> GenerateListButtons(RavioButton b)
        {
            int width = (b.Parent as Layer).Width;
            int height = (b.Parent as Layer).Height;


            b.Left += b.Parent.Left;
            b.Top += b.Parent.Top;

            //List<string> strings = Keyboard.ravioPrediction.FindNextString(b.DefaultKey.ToLower(), 8);

            List<string> strings = GenerateNextString(b.DefaultKey.ToLower());

            if (b.Text.Length > 2)
            {
                b = new RavioButton(b.Parent, b.Text, "", "", "", b.Left, b.Top, ButtonType.Double);

            }

            List<Point> positions = new List<Point>();

            //pulsante in alto a sinistra
            positions.Add(new Point(b.Left - b.Width, b.Top - b.Height));
            //pulsante in alto
            positions.Add(new Point(b.Left, b.Top - b.Height));
            //pulsante in alto a destra
            positions.Add(new Point(b.Right, b.Top - b.Height));
            //pulsante a sinistra
            positions.Add(new Point(b.Left - b.Width, b.Top));
            //pulsante a destra
            positions.Add(new Point(b.Right, b.Top));
            //pulsante in basso a sinistra
            positions.Add(new Point(b.Left - b.Width, b.Bottom));
            //pulsante in basso
            positions.Add(new Point(b.Left, b.Bottom));
            //pulsante a destra in basso
            positions.Add(new Point(b.Right, b.Bottom));
            //pulsante al centro
            positions.Add(new Point(b.Left, b.Top));

            //Schema dei tasti
            // 0 1 2 //
            // 3 8 4 //
            // 5 6 7 //

            //controllo se i tasti 0 3 5 sono fuori dallo schermo a sinistra
            if (positions[5].X < 0 && positions[3].X < 0 && positions[0].X < 0)
            {
                //li sposto dentro lo schermo
                //positions[5] = TranslatePoint(positions[5], 3 * b.Width, 0);
                //positions[3] = TranslatePoint(positions[3], 3 * b.Width, 0);
                //positions[0] = TranslatePoint(positions[0], 3 * b.Width, 0);

                for (int c = 0; c < positions.Count; c++)
                {
                    positions[c] = TranslatePoint(positions[c], b.Width, 0);
                }
            }

            //controllo se i tasti 2 4 7 sono fuori dallo schermo a destra
            if (positions[4].X + b.Width > width && positions[7].X + b.Width > width && positions[2].X + b.Width > width)
            {
                //lio sposto dentro lo schermo
                //positions[4] = TranslatePoint(positions[4], -3 * b.Width, 0);
                //positions[7] = TranslatePoint(positions[7], -3 * b.Width, 0);
                //positions[2] = TranslatePoint(positions[2], -3 * b.Width, 0);
                for (int c = 0; c < positions.Count; c++)
                {
                    positions[c] = TranslatePoint(positions[c], -b.Width, 0);
                }
            }

            //controllo se i tasti 5 6 7 sono fuori dallo schermo in basso
            if (positions[7].Y + b.Height > height && positions[6].Y + b.Height > height && positions[5].Y + b.Height > height)
            {
                //lio sposto dentro lo schermo
                //positions[7] = TranslatePoint(positions[7], 0, -3 * b.Height);
                //positions[6] = TranslatePoint(positions[6], 0, -3 * b.Height);
                //positions[5] = TranslatePoint(positions[5], 0, -3 * b.Height);
                for (int c = 0; c < positions.Count; c++)
                {
                    positions[c] = TranslatePoint(positions[c], 0, -b.Height);
                }
            }

            //controllo se i tasti 0 1 2 sono fuori dallo schermo in alto
            if (positions[0].Y < 0 && positions[1].Y < 0 && positions[2].Y < 0)
            {
                //lio sposto dentro lo schermo
                //positions[0] = TranslatePoint(positions[1], 0, 3 * b.Height);
                //positions[1] = TranslatePoint(positions[1], 0, 3 * b.Height);
                //positions[2] = TranslatePoint(positions[2], 0, 3 * b.Height);

                for (int c = 0; c < positions.Count; c++)
                {
                    positions[c] = TranslatePoint(positions[c], 0, b.Height);
                }
            }




            Point bpos = new Point(b.Left, b.Top);
            List<Point> orderedpos = new List<Point>();
            //ordino la lista per distanza dal button centrale 
            orderedpos = positions.OrderBy(o => Distance(bpos, o)).ToList();

            //lista di button
            List<RavioButton> buttons = new List<RavioButton>();

            ButtonType type;
            //se ci sono più di tre caratteri
            if (b.Text.Length > 2)
            {
                type = ButtonType.Double;
            }
            else
            {
                type = ButtonType.Default;
            }

            RavioButton button;

            for (int c = 0; c < strings.Count; c++)
            {
                string s = strings[c];
                if ((b.Parent as Layer).CapsLock)
                    s = s.ToUpper();
                if ((b.Parent as Layer).Shift)
                    s = s[0].ToString().ToUpper() + s.Substring(1);

                button = new RavioButton(s, "", "", "", positions[c].X, positions[c].Y, type);
                button.FontResizer(1f);
                buttons.Add(button);
            }

            button = new RavioButton(b.Text, "", "", "", positions[8].X, positions[8].Y, type);
            button.FontResizer(1f);
            buttons.Add(button);

            activeRectangle = new Rectangle(positions[0].X-1, positions[0].Y-1, b.Width*3+1, b.Height*3+1);

            return buttons;
        }
        public static List<string> GenerateNextString(string s)
        {
            //elenco delle vocali 
            List<string> vocals = Utilities.ConfigManager.config.vocal;
            //elenco delle consonanti
            List<string> cons = Utilities.ConfigManager.config.cons;

            //controllo q
            if (s[s.Length - 1] == 'q')
            {
                vocals = new List<string>() { "ua", "ue", "ui", "uo" };
            }

            //lista di stringhe
            List<string> nextstrings = new List<string>();

            if (IsLetter(s[s.Length - 1]))
            {
                //prendo l'ultima lettera della stringa s e controllo se è vocale o consonante
                if (!IsVocal(s[s.Length - 1]))
                {
                    foreach (string str in vocals)
                    {
                        nextstrings.Add(s + str);
                    }
                    if (s[s.Length - 1] != 'q')
                    {
                        //aggiungo la doppia
                        nextstrings.Add(s + s[s.Length - 1]);
                    }
                }
                else
                {
                    foreach (string str in cons)
                    {
                        nextstrings.Add(s + str);
                    }

                }

                //se non ho 8 elementei aggiungo pulsanti vuoti o altre lettere
                if (nextstrings.Count < 8)
                {
                    for (int i = nextstrings.Count; i < 8; i++)
                    {
                        nextstrings.Add("");
                    }

                }
            }

            return nextstrings;

        }

        public static bool IsVocal(char c)
        {
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u')
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsLetter(char c)
        {
            return Char.IsLetter(c);
        }

        public static Point TranslatePoint(Point p, int x, int y)
        {
            return new Point(p.X + x, p.Y + y);
        }
        public static double Distance(Point p1, Point p2)
        {
            return (Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }
        #endregion
        #region Campi statici
        Color[] Colors =
        {
            Color.Black,
            Color.DarkRed,
            Color.Red,
            Color.Orange,
            Color.Yellow,
            Color.Green,
            Color.Cyan,
            Color.Blue,
            Color.Violet,
            Color.Magenta,
            Color.DarkMagenta,
            Color.White,
        };
        /// <summary>
        /// Colore della cornice
        /// </summary>
        static Color edgeColor = Utilities.ConfigManager.config.edgeColor;
        /// <summary>
        /// colore del layer
        /// </summary>
        static public Color layerColor = Utilities.ConfigManager.config.layerColor;
        /// <summary>
        /// Trasparenza o no
        /// </summary>
        private static bool isBlurred = Utilities.ConfigManager.config.isBlurred;
        #endregion
        #region Campi privati
        /// <summary>
        /// Campo che indica il livello (di fatto il layer stesso)
        /// </summary>
        private int index;
        /// <summary>
        /// Lista di RavioButton
        /// </summary>
        private List<RavioButton> buttons;
        /// <summary>
        /// Grandezza dei pulsanti
        /// </summary>
        private Size buttonSize;

        /// <summary>
        /// Valore del pulsante shift
        /// </summary>
        private bool shift;
        /// <summary>
        /// Valore del pulsante ctrl
        /// </summary>
        private bool ctrl;
        /// <summary>
        /// Valore del pulsante altGr
        /// </summary>
        private bool altGr;
        /// <summary>
        /// Valore del pulsante capsLock
        /// </summary>
        private bool capsLock;
        /// <summary>
        /// Layer progenitore
        /// </summary>
        private Layer baseLayer;
        /// <summary>
        /// Rettangolo contenente i pulsanti
        /// </summary>
        private static Rectangle activeRectangle;


        #endregion

        #region Costruttori
        /// <summary>
        /// Costruttore di default
        /// </summary>
        public Layer() : base()
        {
            ButtonSize = new Size(RavioButton.defaultDimension, RavioButton.defaultDimension);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        public Layer(Control parent) : this()
        {
            Parent = parent;
            if (Parent is Keyboard)
            {
                index = 0;
                switch (Keyboard.theme)
                {
                    case "Dark":
                        BackColor = Utilities.ColorManager.InvertColor(SystemColors.ControlDark);
                        break;
                    default:
                        BackColor = SystemColors.ControlDark;
                        break;
                }

                /// Per debug rimossa qwerty
                try
                {
                    LayoutFromFile();
                }
                catch
                {
                    QwertyLayoutIT();
                }
                ///DebugLayout();

                ///Trovo il control più a destra per settare size
                Size = new Size(buttons.Max(x => x.Right), buttons.Max(x => x.Bottom));
                //Size = Parent.Size;
                foreach (Control c in buttons)
                {
                    Controls.Add(c);
                }

            }
            BaseLayer = this;
        }

        public Layer(RavioButton raviobutton) : this()
        {
            Parent = raviobutton.Parent;
            BaseLayer = (raviobutton.Parent as Layer).baseLayer;
            index = (Parent as Layer).Index + 1;

            Point layerPosition = new Point();

            buttons = Layer.GenerateListButtons(raviobutton);

            Location = layerPosition;

            foreach (Control c in buttons)
            {
                Controls.Add(c);
            }
            if (isBlurred)
                switch (Keyboard.theme)
                {
                    case "Dark":
                        BackColor = Utilities.ColorManager.InvertColor(Color.FromArgb(255*(100-Utilities.ConfigManager.config.TransparencePercentage)/100, Utilities.ConfigManager.config.blurColor));
                        break;
                    default:
                        BackColor = Color.FromArgb(255 * (100-Utilities.ConfigManager.config.TransparencePercentage) / 100, Utilities.ConfigManager.config.blurColor);
                        break;
                }
            else
                BackColor = Color.Transparent;


            /// Rimosso per debug
            Size = Parent.Size;
            ///Size = ((Parent as Keyboard).LayerList[index-1] as Layer).Size + ButtonSize +ButtonSize;
            Shift = (raviobutton.Parent as Layer).Shift;
            CapsLock = (raviobutton.Parent as Layer).CapsLock;
            (Parent as Layer).Controls.Add(this);
            BringToFront();
            
        }

        #endregion

        #region Proprietà

        /// <summary>
        /// Proprietà che indica il livello (di fatto il layer stesso)
        /// </summary>
        public int Index { get => index; }
        /// <summary>
        /// Grandezza dei pulsanti del layer
        /// </summary>
        public Size ButtonSize { get => buttonSize; set => buttonSize = value; }
        /// <summary>
        /// Valore del pulsante shift
        /// </summary>
        public bool Shift
        {
            get => shift;
            set
            {
                shift = value;
                if (Parent is Layer)
                    (Parent as Layer).Shift = value;
                foreach (RavioButton r in buttons)
                {
                    r.UpdateText();
                    if (r.Command() == "Shift")
                    {
                        r.Invalidate();
                        r.Update();
                    }
                }
            }
        }
        /// <summary>
        /// Valore del pulsante ctrl
        /// </summary>
        public bool Ctrl
        {
            get => ctrl;
            set
            {
                ctrl = value;
                if (Parent is Layer)
                    (Parent as Layer).Ctrl = value;
                foreach (RavioButton r in buttons)
                {
                    r.UpdateText();
                    if (r.Command() == "Ctrl")
                    {
                        r.Invalidate();
                        r.Update();
                    }
                }
            }
        }
        /// <summary>
        /// Valore del pulsante altGr
        /// </summary>
        public bool AltGr
        {
            get => altGr;
            set
            {
                altGr = value;
                if (Parent is Layer)
                    (Parent as Layer).AltGr = value;
                foreach (RavioButton r in buttons)
                {
                    r.UpdateText();
                    if (r.Command() == "AltGr")
                    {
                        r.Invalidate();
                        r.Update();
                    }
                }
            }
        }
        /// <summary>
        /// Valore del pulsante capsLock
        /// </summary>
        public bool CapsLock
        {
            get => capsLock;
            set
            {
                capsLock = value;
                foreach (RavioButton r in buttons)
                {
                    r.UpdateText();
                }
                Invalidate();
                Update();
            }
        }

        public Layer BaseLayer { get => baseLayer; set => baseLayer = value; }
        public Rectangle ActiveRectangle { get => activeRectangle; set => activeRectangle = value; }



        #endregion

        #region Metodi
        /// <summary>
        /// Inizializza la tastiera con layout qwerty italiano
        /// </summary>
        private void QwertyLayoutIT()
        {
            buttons = new List<RavioButton>()
            {
                ///Prima riga
                new RavioButton(this, "\\","|","","", 0, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "1","!","","", ButtonSize.Width*1, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "2","\"","","", ButtonSize.Width*2, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "3","£","","", ButtonSize.Width*3, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "4","$","","", ButtonSize.Width*4, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "5","%","€","", ButtonSize.Width*5, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "6","&","","", ButtonSize.Width*6, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "7","/","","", ButtonSize.Width*7, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "8","(","","", ButtonSize.Width*8, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "9",")","","", ButtonSize.Width*9, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "0","=","","", ButtonSize.Width*10, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "'","?","","", ButtonSize.Width*11, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "ì","ó","","", ButtonSize.Width*12, 0, RavioButton.ButtonType.Default),
                new RavioButton(this, "\b","","","", ButtonSize.Width*13, 0, RavioButton.ButtonType.Double, RavioButton.TextAlign.Right),
                /// 
                ///Seconda riga
                new RavioButton(this, "\t","","","", 0, ButtonSize.Height*1, RavioButton.ButtonType.OneHalf, RavioButton.TextAlign.Left),
                new RavioButton(this, "q","Q","","", (int)(ButtonSize.Width*1.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "w","W","","", (int)(ButtonSize.Width*2.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "e","E","€","", (int)(ButtonSize.Width*3.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "r","R","","", (int)(ButtonSize.Width*4.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "t","T","","", (int)(ButtonSize.Width*5.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "y","Y","","", (int)(ButtonSize.Width*6.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "u","U","","", (int)(ButtonSize.Width*7.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "i","I","","",(int)(ButtonSize.Width*8.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "o","O","","",(int)(ButtonSize.Width*9.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "p","P","","", (int)(ButtonSize.Width*10.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "è","é","[","{",(int)(ButtonSize.Width*11.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "+","*","]","}", (int)(ButtonSize.Width*12.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new RavioButton(this, "\r","","","", (int)(ButtonSize.Width*13.5f), ButtonSize.Height*1, RavioButton.ButtonType.Enter),
                ///
                ///Terza riga
                new RavioButton(this, "CapsLock","","","", 0, ButtonSize.Height*2, RavioButton.ButtonType.OneHalf, RavioButton.TextAlign.Left),
                new RavioButton(this, "a","A","","", (int)(ButtonSize.Width*1.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "s","S","","", (int)(ButtonSize.Width*2.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "d","D","","",(int)(ButtonSize.Width*3.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "f","F","","",(int)(ButtonSize.Width*4.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "g","G","","", (int)(ButtonSize.Width*5.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "h","H","","", (int)(ButtonSize.Width*6.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "j","J","","", (int)(ButtonSize.Width*7.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "k","K","","", (int)(ButtonSize.Width*8.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "l","L","","", (int)(ButtonSize.Width*9.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "ò","ç","@","", (int)(ButtonSize.Width*10.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "à","°","#","", (int)(ButtonSize.Width*11.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new RavioButton(this, "ù","§","","", (int)(ButtonSize.Width*12.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                ///
                ///Quarta riga
                new RavioButton(this, "Shift","","","", 0, ButtonSize.Height*3, RavioButton.ButtonType.Double, RavioButton.TextAlign.Left),
                new RavioButton(this, "<",">","","", (int)(ButtonSize.Width*2), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, "z","Z","","", (ButtonSize.Width*3), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, "x","X","","",(ButtonSize.Width*4), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, "c","C","","", (ButtonSize.Width*5), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, "v","V","","", (ButtonSize.Width*6), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, "b","B","","", (ButtonSize.Width*7), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, "n","N","","", (ButtonSize.Width*8), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, "m","M","","", (ButtonSize.Width*9), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, ",",";","","", (int)(ButtonSize.Width*10), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, ".",":","","", (int)(ButtonSize.Width*11), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, "-","_","","", (int)(ButtonSize.Width*12), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new RavioButton(this, "Shift","","","", (int)(ButtonSize.Width*13), ButtonSize.Height*3, RavioButton.ButtonType.Double, RavioButton.TextAlign.Right),
                ///
                ///Quinta riga
                new RavioButton(this, "Ctrl","","","", 0, ButtonSize.Height*4, RavioButton.ButtonType.Double, RavioButton.TextAlign.Left),
                new RavioButton(this, "TopMostLockTrue","","","", (ButtonSize.Width*2), ButtonSize.Height*4, RavioButton.ButtonType.Default),
                new RavioButton(this, "Settings","","","", (ButtonSize.Width*3), ButtonSize.Height*4, RavioButton.ButtonType.Default),
                new RavioButton(this, "","","","",(ButtonSize.Width*4), ButtonSize.Height*4, RavioButton.ButtonType.Default),
                new RavioButton(this, " "," "," "," ", (ButtonSize.Width*5), ButtonSize.Height*4, RavioButton.ButtonType.Space),
                new RavioButton(this, "AltGr","","","", (ButtonSize.Width*10), ButtonSize.Height*4, RavioButton.ButtonType.Double),
                new RavioButton(this, "","","","", (ButtonSize.Width*12), ButtonSize.Height*4, RavioButton.ButtonType.Default),
                new RavioButton(this, "Ctrl","","","", (ButtonSize.Width*13), ButtonSize.Height*4, RavioButton.ButtonType.Double, RavioButton.TextAlign.Right),

            };

            Utilities.ConfigManager.buttonlist = new List<SerializeButton>();
            /*
            {

                ///Prima riga
                new SerializeButton( "\\","|","","", 0, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "1","!","","", ButtonSize.Width*1, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "2","\"","","", ButtonSize.Width*2, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "3","£","","", ButtonSize.Width*3, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "4","$","","", ButtonSize.Width*4, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "5","%","€","", ButtonSize.Width*5, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "6","&","","", ButtonSize.Width*6, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "7","/","","", ButtonSize.Width*7, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "8","(","","", ButtonSize.Width*8, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "9",")","","", ButtonSize.Width*9, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "0","=","","", ButtonSize.Width*10, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "'","?","","", ButtonSize.Width*11, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "ì","ó","","", ButtonSize.Width*12, 0, RavioButton.ButtonType.Default),
                new SerializeButton( "\b","","","", ButtonSize.Width*13, 0, RavioButton.ButtonType.Double),
                /// 
                ///Seconda riga
                new SerializeButton( "\t","","","", 0, ButtonSize.Height*1, RavioButton.ButtonType.OneHalf),
                new SerializeButton( "q","Q","","", (int)(ButtonSize.Width*1.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "w","W","","", (int)(ButtonSize.Width*2.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "e","E","€","", (int)(ButtonSize.Width*3.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "r","R","","", (int)(ButtonSize.Width*4.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "t","T","","", (int)(ButtonSize.Width*5.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "y","Y","","", (int)(ButtonSize.Width*6.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "u","U","","", (int)(ButtonSize.Width*7.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "i","I","","",(int)(ButtonSize.Width*8.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "o","O","","",(int)(ButtonSize.Width*9.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "p","P","","", (int)(ButtonSize.Width*10.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "è","é","[","{",(int)(ButtonSize.Width*11.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "+","*","]","}", (int)(ButtonSize.Width*12.5f), ButtonSize.Height*1, RavioButton.ButtonType.Default),
                new SerializeButton( "\r","","","", (int)(ButtonSize.Width*13.5f), ButtonSize.Height*1, RavioButton.ButtonType.Enter),
                ///
                ///Terza riga
                new SerializeButton( "CapsLock","","","", 0, ButtonSize.Height*2, RavioButton.ButtonType.OneHalf),
                new SerializeButton( "a","A","","", (int)(ButtonSize.Width*1.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "s","S","","", (int)(ButtonSize.Width*2.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "d","D","","",(int)(ButtonSize.Width*3.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "f","F","","",(int)(ButtonSize.Width*4.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "g","G","","", (int)(ButtonSize.Width*5.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "h","H","","", (int)(ButtonSize.Width*6.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "j","J","","", (int)(ButtonSize.Width*7.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "k","K","","", (int)(ButtonSize.Width*8.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "l","L","","", (int)(ButtonSize.Width*9.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "ò","ç","@","", (int)(ButtonSize.Width*10.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "à","°","#","", (int)(ButtonSize.Width*11.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                new SerializeButton( "ù","§","","", (int)(ButtonSize.Width*12.5f), ButtonSize.Height*2, RavioButton.ButtonType.Default),
                ///
                ///Quarta riga
                new SerializeButton( "Shift","","","", 0, ButtonSize.Height*3, RavioButton.ButtonType.Double),
                new SerializeButton( "<",">","","", (int)(ButtonSize.Width*2), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( "z","Z","","", (ButtonSize.Width*3), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( "x","X","","",(ButtonSize.Width*4), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( "c","C","","", (ButtonSize.Width*5), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( "v","V","","", (ButtonSize.Width*6), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( "b","B","","", (ButtonSize.Width*7), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( "n","N","","", (ButtonSize.Width*8), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( "m","M","","", (ButtonSize.Width*9), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( ",",";","","", (int)(ButtonSize.Width*10), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( ".",":","","", (int)(ButtonSize.Width*11), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( "-","_","","", (int)(ButtonSize.Width*12), ButtonSize.Height*3, RavioButton.ButtonType.Default),
                new SerializeButton( "Shift","","","", (int)(ButtonSize.Width*13), ButtonSize.Height*3, RavioButton.ButtonType.Double),
                ///
                ///Quinta riga
                new SerializeButton( "Ctrl","","","", 0, ButtonSize.Height*4, RavioButton.ButtonType.Double),
                new SerializeButton( "TopMostLockTrue","","","", (ButtonSize.Width*2), ButtonSize.Height*4, RavioButton.ButtonType.Default),
                new SerializeButton( "Settings","","","", (ButtonSize.Width*3), ButtonSize.Height*4, RavioButton.ButtonType.Default),
                new SerializeButton( "","","","",(ButtonSize.Width*4), ButtonSize.Height*4, RavioButton.ButtonType.Default),
                new SerializeButton( " "," "," "," ", (ButtonSize.Width*5), ButtonSize.Height*4, RavioButton.ButtonType.Space),
                new SerializeButton( "AltGr","","","", (ButtonSize.Width*10), ButtonSize.Height*4, RavioButton.ButtonType.Double),
                new SerializeButton( "","","","", (ButtonSize.Width*12), ButtonSize.Height*4, RavioButton.ButtonType.Default),
                new SerializeButton( "Ctrl","","","", (ButtonSize.Width*13), ButtonSize.Height*4, RavioButton.ButtonType.Double)

            };*/
            foreach(RavioButton r in buttons)
            {
                Utilities.ConfigManager.buttonlist.Add(r.ConvertToSerialize());
            }
            ConfigManager.WriteFile(ConfigManager.SerializeButtons(), "keyboardconfig");
            
        }

        private void LayoutFromFile()
        {
            Utilities.ConfigManager.DeserializeButton(Utilities.ConfigManager.ReadFile("keyboardconfig"));

            buttons = new List<RavioButton>();

            foreach(SerializeButton b in ConfigManager.buttonlist)
            {
                buttons.Add(new RavioButton(this, b.Digit, b.Shiftdigit, b.Altdigit, b.Shiftaltdigit, b.Left, b.Top, b.Buttontype,b.Textalign));
            }
        }


        private void DebugLayout()
        {
            buttons = new List<RavioButton>()
            {
                new RavioButton(this, "a","","","",0, 0, RavioButton.ButtonType.Default),
            };
        }
        #endregion

        #region Handler per eventi

        /// <summary>
        /// Handler che disegna lo sfondo del layer
        /// </summary>
        /// <param name="pevent"></param>
        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            pevent.Graphics.FillRectangle(new SolidBrush(BackColor), new Rectangle(Left, Top, Width, Height));
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.DrawRectangle(new Pen(edgeColor, 5f), ActiveRectangle);
        }
        /// <summary>
        /// Handler evento click up:
        /// Valido il controllo (uguale a invalido)
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            Invalidate(true);
            Update();
            base.OnMouseUp(e);
            int index = GetIndexLayer(e.Location);
            DisposeLayers(0);

        }
        /// <summary>
        /// Handler evento click down:
        /// Mouse premuto-> IsPressed = true
        /// Invalido il controllo
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            Invalidate();
            Update();
            base.OnMouseDown(e);
        }


        #endregion

        #region Utilities

        /// <summary>
        /// /Funzione che elimina tutti i layer
        /// </summary>
        public void DisposeLayers(int index = 0)
        {
            if (Parent is Layer)
                if ((Parent as Layer).Index > index)
                {
                    (Parent as Layer).DisposeLayers(index);
                }
                else
                {
                    Keyboard.prevbutton = null;
                    (Parent as Layer).Shift = false;
                    //Keyboard.OnDisposingLayer(this, new EventArgs());
                    Dispose(true);
                    Invalidate(true);
                    Update();

                }

        }

        public int GetIndexLayer(Point mousePos)
        {
            if (Parent is Layer)
            {
                foreach (RavioButton b in (Parent as Layer).buttons)
                {
                    if (Rectangle.Intersect(new Rectangle(mousePos, new Size(1, 1)), b.Bounds) != Rectangle.Empty)
                        return (Parent as Layer).Index;

                }
                return (Parent as Layer).GetIndexLayer(mousePos);
            }

            return 0;

        }


        #endregion
    }
}

